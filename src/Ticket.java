public class Ticket{
    private String nama;
    private String asal;
    private String tujuan;

//konstruktor tiket yang hanya ada nama saja
    public Ticket (String nama){
        this.nama= nama;
    }

//konstruktor tiket untuk nama, asal, dan tujuan
    public Ticket (String n, String a, String t){
        this.nama = n;
        this.asal = a;
        this.tujuan = t;
    }

//method menampilkan info dari tiket
//if sebagai kondisi ketika asal dan tujuan null serta else untuk kondisi yg tidak memenuhi kondisi if
    public void infoTiket(){
        if (asal == null && tujuan == null){
            System.out.println("Nama: "+this.nama);
        }
        else{
            System.out.println("Nama:"+this.nama);
            System.out.println("Asal:"+this.asal);
            System.out.println("Tujuan:"+this.tujuan);
            System.out.println("-----------------------------------------");
        }

    }
}
public class Kereta {
    private String namaKereta;
    private int totalTiket;
    private Ticket [] tiket;

//constructor kereta komuter
    public Kereta(){
        this.namaKereta = "komuter";
        this.totalTiket = 1000;
        tiket = new Ticket[this.totalTiket];
    }

//konstruktor kreta lain (KAJJ)
    public Kereta (String n, int tt){
        this.namaKereta= n;
        this.totalTiket= tt;
        tiket = new Ticket [this.totalTiket];
    }

//method menambahkan tiket dari nama kereta (komuter)
//if untuk syarat ketersediaan tiket
//for untuk mengisi list dari tiket dalam array
//increment totalTiket untuk mengurangi jumlah tiket yg telah dipesan
//if untuk menampilkan sisa tiket yang belum dipesan ketika totalTiket kurang dari 30
    public void tambahTiket(String n) {
        if (totalTiket > 0) {
            for (int i = 0; i < tiket.length; i++) {
                if (tiket[i] != null) {
                    continue;
                } else {
                    this.tiket[i] = new Ticket(n);
                    break;
                }
            }  
            this.totalTiket--;
            System.out.println("================================================================");
            System.out.print("Tiket berhasil dipesan");

            if (totalTiket < 30 && totalTiket >= 0) {
                System.out.print(". Jumlah tiket teresdia: " +totalTiket);
            }
            System.out.println();
        }
        else {
            System.out.println("================================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

//method menambahkan tiket dari nama kereta lain (KAJJ)
//if untuk syarat ketersediaan tiket
//for untuk mengisi list dari tiket dalam array
//increment totalTiket untuk mengurangi jumlah tiket yg telah dipesan
//if untuk menampilkan sisa tiket yang belum dipesan ketika totalTiket kurang dari 30
    public void tambahTiket(String n, String a, String t) {
        if (totalTiket > 0) {
            for (int i = 0; i < tiket.length; i++) {
                if (tiket[i] != null) {
                    continue;
                } else {
                    tiket[i] = new Ticket(n, a, t);
                    break;
                }
            } 
            this.totalTiket--;
            System.out.println("================================================================");
            System.out.print("Tiket berhasil dipesan");

            if (totalTiket < 30 && totalTiket >= 0) {
                System.out.print(". Jumlah tiket teresdia: " +totalTiket);
            }
            System.out.println();
        }
        else {
            System.out.println("================================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

//method info detail dari tiket pada array
//for untuk menampilkan penumpang dengan tiket
//if untuk menampilkan tiket yang telah dipesan saja
    public void tampilkanTiket(){
        System.out.println("========================================");
        System.out.println("Daftar penumpang kereta api " +this.namaKereta +":");
        System.out.println("----------------------------------------");

        for (Ticket tiket : tiket) {  
            if (tiket == null) {
                break;
            } 
            else {
                tiket.infoTiket();
            }
        }

    }

}